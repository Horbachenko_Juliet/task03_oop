package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.House;
import com.epam.model.Model;

import java.util.List;

public class ComtrollerImpl implements Controller {
    private Model model;

    public ComtrollerImpl(){
        model = new BusinessLogic();
    }

    @Override
    public List<House> getHousingList() {
        return model.getHousingList();
    }

    @Override
    public List<House> sortHousingListByPrice(int amount) {
        return model.sortHousingListByPrice(amount);
    }

    @Override
    public List<House> sortHousingListByDistanceToKindergarten(int amount) {
        return model.sortHousingListByDistanceToKindergarten(amount);
    }

    @Override
    public List<House> sortHousingListByDistanceToSchool(int amount) {
        return model.sortHousingListByDistanceToSchool(amount);
    }

    @Override
    public List<House> sortHousingListByDistanceToPlayground(int amount) {
        return model.sortHousingListByDistanceToPlayground(amount);
    }
}
