package com.epam.controller;

import com.epam.model.House;

import java.util.List;

public interface Controller {
    List<House> getHousingList();
    List<House> sortHousingListByPrice(int amount);
    List<House> sortHousingListByDistanceToKindergarten(int amount);
    List<House> sortHousingListByDistanceToSchool(int amount);
    List<House> sortHousingListByDistanceToPlayground(int amount);
}
