package com.epam.model;

import java.util.List;

public class BusinessLogic implements Model {

    private House housing;


    public BusinessLogic() {
        housing = new House();
    }
    @Override
    public List<House> getHousingList() {
        return housing.getHouseList();
    }

    @Override
    public List<House> sortHousingListByPrice(int amount) {
        return housing.sortHousingListByPrice(amount);
    }

    @Override
    public List<House> sortHousingListByDistanceToKindergarten(int amount) {
        return housing.sortHousingListByDistanceToKindergarten(amount);
    }

    @Override
    public List<House> sortHousingListByDistanceToSchool(int amount) {
        return housing.sortHousingListByDistanceToSchool(amount);
    }

    @Override
    public List<House> sortHousingListByDistanceToPlayground(int amount) {
        return housing.sortHousingListByDistanceToPlayground(amount);
    }
}
