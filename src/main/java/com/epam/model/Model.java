package com.epam.model;

import java.util.List;

public interface Model {

    List<House> getHousingList();
    List<House> sortHousingListByPrice(int amount);
    List<House> sortHousingListByDistanceToKindergarten(int amount);
    List<House> sortHousingListByDistanceToSchool(int amount);
    List<House> sortHousingListByDistanceToPlayground(int amount);
}
