package com.epam.model.Comparator;

import com.epam.model.House;

import java.util.Comparator;

public class PriceComparator implements Comparator<House> {
    @Override
    public int compare(House h1, House h2) {
        return Integer.compare(h1.getPriceOfRent(), h2.getPriceOfRent());
    }
}
