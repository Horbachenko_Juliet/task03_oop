package com.epam.model.Comparator;

import com.epam.model.House;

import java.util.Comparator;

public class DistanceToPlaygroundComparator implements Comparator <House> {
    @Override
    public int compare(House h1, House h2) {
        return Integer.compare(h1.getDisToPlayground(), h2.getDisToPlayground());
    }
}
