package com.epam.model.HouseType;

import com.epam.model.House;

import java.util.Random;

public class Mansion extends House {
    public Mansion() {
        Random rnd = new Random();
        this.setPriceOfRent((11 * rnd.nextInt() + 5) * 100);
        this.setDisToKindergarten((10 * rnd.nextInt() + 2) * 100);
        this.setDisToSchool((11 * rnd.nextInt() + 5) * 100);
        this.setDisToPlayground((4 * rnd.nextInt() + 1) * 100);
    }
}
