package com.epam.model.HouseType;

import com.epam.model.House;

import java.util.Random;

public class Apartment extends House {
    public Apartment() {
        Random rnd = new Random();
        this.setPriceOfRent(((int)(3.51 * rnd.nextInt() + 1.5)) * 100);
        this.setDisToKindergarten((10 * rnd.nextInt() + 2) * 100);
        this.setDisToSchool((11 * rnd.nextInt() + 5) * 100);
        this.setDisToPlayground((4 * rnd.nextInt() + 1) * 100);
    }
}
