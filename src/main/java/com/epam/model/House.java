package com.epam.model;

/**
 * Class create List of houses.
 */

import com.epam.model.Comparator.DistanceToKindergartenComparetor;
import com.epam.model.Comparator.DistanceToPlaygroundComparator;
import com.epam.model.Comparator.DistanceToSchoolComparator;
import com.epam.model.Comparator.PriceComparator;
import com.epam.model.HouseType.Apartment;
import com.epam.model.HouseType.Mansion;
import com.epam.model.HouseType.Penthouse;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class House {

    public List<House> sortedList;
    private int priceOfRent;
    private int disToKindergarten;
    private int disToSchool;
    private int disToPlayground;
    private List<House> houseList;

    public House() {
        houseList = new LinkedList<>();
        generateHousingList();
    }

    public int getPriceOfRent() {
        return priceOfRent;
    }

    public void setPriceOfRent(int priceOfRent) {
        this.priceOfRent = priceOfRent;
    }

    public int getDisToKindergarten() {
        return disToKindergarten;
    }

    public void setDisToKindergarten(int disToKindergarten) {
        this.disToKindergarten = disToKindergarten;
    }

    public int getDisToSchool() {
        return disToSchool;
    }

    public void setDisToSchool(int disToSchool) {
        this.disToSchool = disToSchool;
    }

    public int getDisToPlayground() {
        return disToPlayground;
    }

    public void setDisToPlayground(int disToPlayground) {
        this.disToPlayground = disToPlayground;
    }

    private void generateHousingList() {
       Random rnd = new Random();
        for (int i = 0; i < 10; i++) {
            switch (rnd.nextInt() * 3) {
                case 0:
                    houseList.add(new Apartment());
                    break;
                case 1:
                    houseList.add(new Mansion());
                    break;
                case 2:
                    houseList.add(new Penthouse());
                    break;
                default:
                    System.out.println("Error in HouseList creation");
                    break;
            }
        }
    }

    public List<House> getHouseList() {
        return houseList;
    }

    public List<House> sortHousingListByPrice(int amount) {
        sortedList = new LinkedList<>();
        for (House h : houseList) {
            if (h.getPriceOfRent() < amount) {
                houseList.add(h);
            }
            PriceComparator myPriceComparator = new PriceComparator();
            houseList.sort(myPriceComparator);
        }
        return houseList;
    }

    public List<House> sortHousingListByDistanceToKindergarten(int amount) {
        sortedList = new LinkedList<>();
        for (House h : houseList) {
            if (h.getDisToKindergarten() < amount) {
                houseList.add(h);
            }
            DistanceToKindergartenComparetor myKindergartenComp = new DistanceToKindergartenComparetor();
            houseList.sort(myKindergartenComp);
        }
        return houseList;
    }

    public List<House> sortHousingListByDistanceToSchool(int amount) {
        sortedList = new LinkedList<>();
        for (House h : houseList) {
            if (h.getDisToSchool() < amount) {
                houseList.add(h);
            }
            DistanceToSchoolComparator mySchoolComp = new DistanceToSchoolComparator();
            houseList.sort(mySchoolComp);
        }
        return houseList;
    }

    public List<House> sortHousingListByDistanceToPlayground(int amount) {
        sortedList = new LinkedList<>();
        for (House h : houseList) {
            if (h.getDisToPlayground() < amount) {
                houseList.add(h);
            }
            DistanceToPlaygroundComparator myPlaygroundComp = new DistanceToPlaygroundComparator();
            houseList.sort(myPlaygroundComp);
        }
        return houseList;
    }
}
