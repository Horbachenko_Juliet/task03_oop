package com.epam.view;

import com.epam.controller.ComtrollerImpl;
import com.epam.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ComtrollerImpl();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", " 1 - print housing list");
        menu.put("2", " 2 - sort housing list by price");
        menu.put("3", " 3 - sort housing list by distance to kindergarten");
        menu.put("4", " 4 - sort housing list by distance to school");
        menu.put("5", " 5 - sort housing list by distance to playground");

        this.methodsMenu = new LinkedHashMap<String, Printable>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        System.out.println(controller.getHousingList());
    }

    private void pressButton2() {
        System.out.println("Please input maximum price:");
        int amount = input.nextInt();
        System.out.println(controller.sortHousingListByPrice(amount));
    }

    private void pressButton3() {
        System.out.println("Please input maximum distance to kindergarten:");
        int amount = input.nextInt();
        System.out.println(controller.sortHousingListByDistanceToKindergarten(amount));
    }

    private void pressButton4() {
        System.out.println("Please input maximum distance to school:");
        int amount = input.nextInt();
        System.out.println(controller.sortHousingListByDistanceToSchool(amount));
    }

    private void pressButton5() {
        System.out.println("Please input maximum distance to playground:");
        int amount = input.nextInt();
        System.out.println(controller.sortHousingListByDistanceToPlayground(amount));
    }

    //--------------------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
